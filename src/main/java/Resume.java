public class Resume {
    private String firstName;
    private String lastName;
    private String porfesion;
    private String education;
    private String summary;

    public Resume(String firstName, String lastName, String porfesion, String education, String summary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.porfesion = porfesion;
        this.education = education;
        this.summary = summary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPorfesion() {
        return porfesion;
    }

    public void setPorfesion(String porfesion) {
        this.porfesion = porfesion;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
