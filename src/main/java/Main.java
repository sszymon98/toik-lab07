import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class Main {
    public static void main(String[] args) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("Hello.pdf"));

            document.open();

            addTitle(document);
            addTable(document);

            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void addTitle(Document document) throws DocumentException {
        Paragraph paragraph = new Paragraph("Resume");
        paragraph.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(paragraph, 3);

        document.add(paragraph);
    }

    private static void addTable(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        table.addCell("First name");
        table.addCell("Szymon");

        table.addCell("Last name");
        table.addCell("Biliński");

        table.addCell("Profesion");
        table.addCell("Student");

        table.addCell("Education");
        table.addCell("PWSZ w Tarnowie");

        table.addCell("Summary");
        table.addCell("Summary will be soon!");
        document.add(table);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
